UTF-8
### Candidate Chinese Name:
* 
 秦明磊
- - -  
### Please write down some feedback about the question(could be in Chinese):
*
运用抽象工厂模式，MqFactory是工厂接口。factory.impl包下的其他类是工厂类。遵循开闭原则和单一原则。
JmsMessageBroker.java是消息队列接口类，分别有ActiveMq、IbmMq、Tibco消息队列实现类。因本次是重构，顾IbmMq和TibcoMq并未实现。
测试类遵循单一原则，分别对三种消息队列创建了测试类。经过重构使此接口拓展性较好。
必要的注释有助于代码维护和阅读。
- - -