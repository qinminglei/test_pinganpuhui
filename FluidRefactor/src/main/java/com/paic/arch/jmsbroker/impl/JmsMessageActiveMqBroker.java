package com.paic.arch.jmsbroker.impl;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import com.paic.arch.jmsbroker.JmsMessageBroker;

import javax.jms.*;

import java.lang.IllegalStateException;

import static org.slf4j.LoggerFactory.getLogger;
import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

public class JmsMessageActiveMqBroker implements JmsMessageBroker{
    private static final Logger LOG = getLogger(JmsMessageActiveMqBroker.class);
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private String brokerUrl;
    private BrokerService brokerService;
    
    public JmsMessageActiveMqBroker(){
    }

    private JmsMessageActiveMqBroker(String aBrokerUrl) {
        brokerUrl = aBrokerUrl;
    }

    /**
     * 创建消息队列链接，指定端口范围内
     * @return
     * @throws Exception
     */
    public JmsMessageActiveMqBroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    }

    /**
     * 创建消息队列链接     
     * * @param aBrokerUrl
     * @return
     * @throws Exception
     */
    public JmsMessageActiveMqBroker createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        JmsMessageActiveMqBroker broker = bindToBrokerAtUrl(aBrokerUrl);
        broker.createEmbeddedBroker();
        broker.startEmbeddedBroker();
        return broker;
    }

    /**
     * 创建消息队列链接
     * @throws Exception
     */
    private void createEmbeddedBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }

    public JmsMessageActiveMqBroker bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
    	 brokerUrl = aBrokerUrl;
        return this;
    }

    /**
     * 启动消息队列链接
     * @throws Exception
     */
    private void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }

    /**
     * 关闭消息队列链接
     * @throws Exception
     */
    public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    public JmsMessageActiveMqBroker andThen() {
        return this;
    }

    public String getBrokerUrl() {
        return brokerUrl;
    }

    /**
     * 发送消息
     * @param aDestinationName
     * @param aMessageToSend
     * @return
     */
    public JmsMessageActiveMqBroker sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
        executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(aMessageToSend));
            producer.close();
            return "";
        });
        return this;
    }

    /**
     * 接收消息
     * @param aDestinationName
     * @return
     */
    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    /**
     * 接收消息
     * @param aDestinationName
     * @param aTimeout
     * @return
     */
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(aTimeout);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            consumer.close();
            return ((TextMessage) message).getText();
        });
    }

    /**
     * 链接消息队列执行Callback
     * @param aBrokerUrl
     * @param aDestinationName
     * @param aCallback
     * @return
     */
    private String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName, JmsCallback aCallback) {
        Connection connection = null;
        String returnValue = "";
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
            connection = connectionFactory.createConnection();
            connection.start();
            returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", aBrokerUrl);
            throw new IllegalStateException(jmse);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
                    throw new IllegalStateException(jmse);
                }
            }
        }
        return returnValue;
    }

    interface JmsCallback {
        String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;
    }

    /**
     * 获得消息队列链接session和Queue,并执行Callback
     * @param aConnection
     * @param aDestinationName
     * @param aCallback
     * @return
     */
    private String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName, JmsCallback aCallback) {
        Session session = null;
        try {
            session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(aDestinationName);
            return aCallback.performJmsFunction(session, queue);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", aConnection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }

    /**
     * 获得消息队列深度
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }

    /**
     * 指定消息队列是否为空
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
        return getEnqueuedMessageCountAt(aDestinationName) == 0;
    }

    /**
     * 统计指定队列消息数
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }

    public class NoMessageReceivedException extends RuntimeException {
        public NoMessageReceivedException(String reason) {
            super(reason);
        }
    }
}
