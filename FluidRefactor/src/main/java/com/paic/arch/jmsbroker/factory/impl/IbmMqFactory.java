package com.paic.arch.jmsbroker.factory.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.paic.arch.jmsbroker.JmsMessageBroker;
import com.paic.arch.jmsbroker.factory.MqFactory;
import com.paic.arch.jmsbroker.impl.JmsMessageIbmMqBroker;

/**
 * IbmMq工厂类
 * 
 * @author qinminglei
 * @version 1.0 2018-02-01
 */
public class IbmMqFactory implements MqFactory {

	@Autowired
	private JmsMessageBroker jmsMessageIbmMqBroker;

	/**
	 * 创建 IbmMqBroker对象
	 */
	@Override
	public JmsMessageBroker produce() throws Exception {
		if (jmsMessageIbmMqBroker != null) {
			return jmsMessageIbmMqBroker;
		}
		return new JmsMessageIbmMqBroker();
	}

}
