package com.paic.arch.jmsbroker.factory.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.paic.arch.jmsbroker.JmsMessageBroker;
import com.paic.arch.jmsbroker.factory.MqFactory;
import com.paic.arch.jmsbroker.impl.JmsMessageTibcoMqBroker;

/**
 * TibcoMq工厂类
 * 
 * @author qinminglei
 * @version 1.0 2018-02-01
 */
public class TibcoMqFactory implements MqFactory {
	
	@Autowired
	private JmsMessageBroker jmsMessageTibcoMqBroker;

	/**
	 * 创建 TibcoMqBroker对象
	 */
	@Override
	public JmsMessageBroker produce() throws Exception {
		if (jmsMessageTibcoMqBroker != null) {
			return jmsMessageTibcoMqBroker;
		}
		return new JmsMessageTibcoMqBroker();
	}

}
