package com.paic.arch.jmsbroker.factory;

import com.paic.arch.jmsbroker.JmsMessageBroker;

/**
 * 抽象工厂
 * @author qinminglei
 * @version 1.0 2018-02-01
 */
public interface MqFactory {
	public JmsMessageBroker produce()throws Exception;  
}
