package com.paic.arch.jmsbroker;

public interface JmsMessageBroker {
	public JmsMessageBroker bindToBrokerAtUrl(String aBrokerUrl) throws Exception;
	public JmsMessageBroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception;
	public String getBrokerUrl();
	public void stopTheRunningBroker() throws Exception;
	public String retrieveASingleMessageFromTheDestination(String aDestinationName);
	public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout);
	public JmsMessageBroker sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend);
	public JmsMessageBroker andThen();
	public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;
}
