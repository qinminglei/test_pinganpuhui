package com.paic.arch.interviews.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paic.arch.interviews.TimeConverter;

/**
 * Set Theory Clock
 * 
 * @author qinminglei
 * @version 1.0 2018-02-01
 */

public class TimeConverterImpl implements TimeConverter {
	private static final Logger LOG = LoggerFactory
			.getLogger(TimeConverterImpl.class);
	/**
	 * �Ƶ�
	 */
	private final String  yellow = "Y";

	/**
	 * ���
	 */
	private final String red = "R";

	/**
	 * ��Ϩ��
	 */
	private final String out = "O";

	/**
	 * ���з�
	 */
	private final String lineBreak = "\r\n";

	/**
	 * ʱ��ת��
	 */
	@Override
	public String convertTime(String aTime) {
		int hours = 24;
		Calendar calendar = new GregorianCalendar();
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		try {
			calendar.setTime(df.parse(aTime));
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			LOG.warn(sw.toString());
			return "";
		}
		if (!aTime.startsWith("24")) {
			hours = calendar.get(Calendar.HOUR_OF_DAY);
		}
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);
		StringBuffer clockHours = convertHours(hours);
		StringBuffer clockMinutes = convertMinutes(minutes);
		StringBuffer clockSeconds = convertSeconds(seconds);
		return clockSeconds.append(lineBreak).append(clockHours)
				.append(lineBreak).append(clockMinutes).toString();
	}

	/**
	 * Сʱת��
	 * 
	 * @param hours
	 * @return
	 */
	public StringBuffer convertHours(int hours) {
		StringBuffer firstRow = new StringBuffer();
		StringBuffer secondRow = new StringBuffer();
		for (int i = 0; i < 4; i++) {
			if (i < hours / 5) {
				firstRow.append(red);
			} else {
				firstRow.append(out);
			}
			if (i < hours % 5) {
				secondRow.append(red);
			} else {
				secondRow.append(out);
			}
		}
		return firstRow.append(lineBreak).append(secondRow);
	}

	/**
	 * ����ת��
	 * 
	 * @param minutes
	 * @return
	 */
	public StringBuffer convertMinutes(int minutes) {
		StringBuffer thirdRow = new StringBuffer();
		StringBuffer fourthRow = new StringBuffer();
		for (int i = 0; i < 11; i++) {
			if (i < minutes / 5) {
				if ((i + 1) % 3 == 0) {
					thirdRow.append(red);
				} else {
					thirdRow.append(yellow);
				}
			} else {
				thirdRow.append(out);
			}
		}
		for (int i = 0; i < 4; i++) {
			if (i < minutes % 5) {
				fourthRow.append(yellow);
			} else {
				fourthRow.append(out);
			}
		}
		return thirdRow.append(lineBreak).append(fourthRow);
	}

	/**
	 * ��ת��
	 * 
	 * @param seconds
	 * @return
	 */
	public StringBuffer convertSeconds(int seconds) {
		if (seconds % 2 != 0) {
			return new StringBuffer(out);
		} else {
			return new StringBuffer(yellow);
		}
	}

}
