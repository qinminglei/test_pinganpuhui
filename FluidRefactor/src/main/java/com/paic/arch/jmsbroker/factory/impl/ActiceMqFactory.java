package com.paic.arch.jmsbroker.factory.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.paic.arch.jmsbroker.JmsMessageBroker;
import com.paic.arch.jmsbroker.factory.MqFactory;
import com.paic.arch.jmsbroker.impl.JmsMessageActiveMqBroker;

/**
 * ActiceMq工厂类
 * 
 * @author qinminglei
 * @version 1.0 2018-02-01
 */
public class ActiceMqFactory implements MqFactory {

	@Autowired
	private JmsMessageBroker jmsMessageActiveMqBroker;

	/**
	 * 创建 ActiveMqBroker对象
	 */
	@Override
	public JmsMessageBroker produce() throws Exception {
		if (jmsMessageActiveMqBroker != null) {
			return jmsMessageActiveMqBroker;
		}
		return new JmsMessageActiveMqBroker();
	}

}
