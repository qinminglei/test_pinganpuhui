package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.JmsMessageBroker;

public class JmsMessageIbmMqBroker implements JmsMessageBroker {
	
	@Override
	public JmsMessageBroker bindToBrokerAtUrl(String aBrokerUrl)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JmsMessageBroker createARunningEmbeddedBrokerOnAvailablePort()
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBrokerUrl() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void stopTheRunningBroker() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String retrieveASingleMessageFromTheDestination(
			String aDestinationName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String retrieveASingleMessageFromTheDestination(
			String aDestinationName, int aTimeout) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JmsMessageBroker sendATextMessageToDestinationAt(
			String aDestinationName, String aMessageToSend) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JmsMessageBroker andThen() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getEnqueuedMessageCountAt(String aDestinationName)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}
	
	 public class NoMessageReceivedException extends RuntimeException {
	        public NoMessageReceivedException(String reason) {
	            super(reason);
	        }
	    }

}
